import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
 
export interface Car {
  id?: string;
  carModel: string;
  carColor: string;
  createdAt: string;
}

@Injectable({
  providedIn: 'root'
})
export class CarService {
  private carsCollection: AngularFirestoreCollection<Car>;
  private cars: Observable<Car[]>;
 
  constructor(public db:AngularFirestore) { 

    this.carsCollection = db.collection<Car>('cars');
    this.cars = this.carsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
  getCars() {
    return this.cars;
  }
 
  getCar(id) {
    return this.carsCollection.doc<Car>(id).valueChanges();
  }
 
  updateCar(car: Car, id: string) {
    return this.carsCollection.doc(id).update(car);
  }
 
  addCar(car: Car) {
    return this.carsCollection.add(car);
  }
 
  removeCar(id) {
    return this.carsCollection.doc(id).delete();
  }
}

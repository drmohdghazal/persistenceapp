import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Todo, TodoService } from '../services/todo.service';
import { Car, CarService } from '../services/cars.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  todosLocal: Todo[];
  todosOnline: Todo[];
  carsOnline:Car[];
  todo: Todo = {
    task: "",
    createdAt: "",
    priority: "" 
  };
    constructor(public storage: Storage,
      private todoService: TodoService,
      private carService: CarService){
      this.todosLocal=[];
      this.todosOnline =[];
      this.carsOnline = []; 
      this.carService.addCar({
        carModel:"Nissan",
        carColor:"Black",
        createdAt:"2012"
      });
    }
    ngOnInit() {
      this.todoService.getTodos().subscribe(res => {
        this.todosOnline = res;
      });

      this.carService.getCars().subscribe(res=>{
        this.carsOnline = res; 
        
       
      });

      this.storage.get("todosLocal").then(res=>{
        if (res != null)
          this.todosLocal = res;
      });
       
  
   


    }
   
    removeTodoOnline(item) {
      this.todoService.removeTodo(item.id);
    }
    removeTodoLocal(itemIndex) {
      this.todosLocal.splice(itemIndex,1);
      this.storage.set("todosLocal",this.todosLocal);
    }
    saveTodoOnline(){
      if(this.todo.task != ""){
        this.todo.createdAt = new Date().getTime().toString();
        this.todoService.addTodo({task:this.todo.task,priority:this.todo.priority,createdAt:this.todo.createdAt});
        
        //clean up 
        this.todo.task = "";
        this.todo.priority = "";
        this.todo.createdAt = ""; 
      }
    }

    saveTodoLocal(){
      if(this.todo.task != ""){
        this.todo.createdAt = new Date().getTime().toString();
        this.todosLocal.push({task:this.todo.task,priority:this.todo.priority,createdAt:this.todo.createdAt});
        this.storage.set("todosLocal",this.todosLocal);
        
        //clean up 
        this.todo.task = "";
        this.todo.priority = "";
        this.todo.createdAt = ""; 
      }
    }

}
